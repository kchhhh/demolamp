(function () {
    'use strict';

    angular.module('helloCoreApi').controller('powerCtrl', [
        '$scope',
        'c8yBase',
        'c8yRealtime',
        '$http',
        '$rootScope',
        '$q',
        'c8yDeviceControl',
        'c8yInventory',
        '$cacheFactory',
        'c8yDevices',
        'c8yCepModuleExamples',
        '$timeout',
        'c8yCepModule',
        'c8yMeasurements',
        powerCtrl
    ]);

    function powerCtrl($scope, c8yBase, c8yRealtime, $http, $rootScope, $q, c8yDeviceControl, c8yInventory, $cacheFactory, c8yDevices, c8yCepModuleExamples, $timeout, c8yCepModule, c8yMeasurements) {
        $scope.deviceId = 13573;
        c8yDevices.detail($scope.deviceId).then(function (res) {
            $scope.device = res.data;
            $scope.name = $scope.device.name;
        });
        $scope.stbd = 20;
        // $scope.pwm = 50;
// $rootScope.arr = [];


        // $scope.aArr = [100, 430, 800, 430];
        // var i = 0;
        // setInterval(function () {
        //     $scope.anum = $scope.aArr[i];
        //     if (i < 3) {
        //         i++;
        //     } else if (i == 3) {
        //         i = 0;
        //     }
        //     // console.log($scope.num);
        // }, 1000);
        setInterval(function () {
            c8yMeasurements.list(
                angular.extend(c8yBase.timeOrderFilter(), {})
            ).then(function (measurements) {
                $scope.measurements = measurements;
                // console.log($scope.measurements);
                angular.forEach($scope.measurements, function (data) {
                    if (data.c8y_smartLEDMeasurement) {
                        $scope.smart = data.c8y_smartLEDMeasurement;
                        $scope.power = $scope.smart.Power.value;
                    }
                });
                console.log($scope.power);
                $rootScope.lampNum = $scope.power/5.5;

            });
        }, 1000*30);

        $(function () {
            $(document).ready(function () {
                Highcharts.setOptions({
                    global: {
                        useUTC: false
                    }
                });
                $('#container').highcharts({
                    chart: {
                        type: 'line',
                        animation: Highcharts.svg, // don't animate in old IE
                        marginRight: 10,
                        backgroundColor: '#3daaff',
                        events: {
                            load: function () {
                                // set up the updating of the chart each second
                                var series = this.series[0];
                                setInterval(function () {
                                    console.log($scope.lampNum);
                                    var x = (new Date()).getTime(), // current time
                                        y = $scope.lampNum;
                                    series.addPoint([x, y], true, true);
                                }, 1000*30);
                            }
                        }
                    },
                    colors: ['red'],
                    title: {
                        text: '实时功率'
                    },
                    xAxis: {
                        type: 'datetime',
                        tickPixelInterval: 150,
                    },
                    yAxis: { //设置Y轴
                        title: '',
                        max: 150, //Y轴最大值
                        min: 0  //Y轴最小值
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>' +
                                Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                                Highcharts.numberFormat(this.y, 2);
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    series: [{
                        name: '实时功率',
                        dashStyle: 'dot',
                        marker: {
                            radius: 3//曲线点半径，默认是4
                        },
                        data: (function () {
                            // generate an array of random data
                            var data = [],
                                time = (new Date()).getTime(),
                                i;
                            for (i = -19; i <= 0; i += 1) {
                                data.push({
                                    x: time + i * 1000,
                                    y: $scope.lampNum
                                });
                            }
                            return data;
                        }())
                    }]
                });
            });
        }());


        //控制灯的operation
        $scope.powerArr = [10, 43, 80, 43];
        var i = 0;
        setInterval(function () {
            $scope.num = $scope.powerArr[i];
            if (i < 3) {
                i++;
            } else if (i == 3) {
                i = 0;
            }
            // console.log($scope.num);
        }, 1000 * 180);

        setInterval(function () {
            var operation = {
                id: 13597,
                "deviceId": "13573",
                "com_goldenpool_model_smartDevice": {
                    "name": "smart device Control",
                    "parameters": {
                        "STDB": "20",
                        "PWM": String($scope.num),
                        "other": "tbd"
                    }
                },
                status: c8yDeviceControl.status.PENDING
            };
            c8yDeviceControl.update(operation);
            c8yDeviceControl.list().then(function (operations) {
                $scope.operations = [];
                angular.forEach(operations, function (operation) {
                    $scope.operations.push(operation);

                });
                console.log($scope.operations);
            });
        }, 1000 * 180);
        $(function () {
            $(document).ready(function () {
                Highcharts.setOptions({
                    global: {
                        useUTC: false
                    }
                });
                $('#container2').highcharts({
                    chart: {
                        type: 'line',
                        animation: Highcharts.svg, // don't animate in old IE
                        marginRight: 10,
                        backgroundColor: '#ffe88b',
                        events: {
                            load: function () {
                                // set up the updating of the chart each second
                                var series = this.series[0];
                                setInterval(function () {
                                    // console.log($scope.power);
                                    var x = (new Date()).getTime(), // current time
                                        y = $scope.num;
                                    series.addPoint([x, y], true, true);
                                }, 1000 * 180);
                            }
                        }
                    },
                    colors: ['red'],
                    plotOptions: {
                        series: {
                            animation: false
                        }
                    },
                    title: {
                        text: '调光功率'
                    },
                    xAxis: {
                        type: 'datetime',
                        tickPixelInterval: 150
                    },
                    yAxis: { //设置Y轴
                        title: '',
                        max: 100, //Y轴最大值
                        min: 0  //Y轴最小值
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>' +
                                Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                                Highcharts.numberFormat(this.y, 2);
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    series: [{
                        name: '调光功率',
                        dashStyle: 'dot',
                        marker: {
                            radius: 3//曲线点半径，默认是4
                        },
                        data: (function () {
                            // generate an array of random data
                            var data = [],
                                time = (new Date()).getTime(),
                                i;
                            for (i = -19; i <= 0; i += 1) {
                                data.push({
                                    x: time + i * 1000,
                                    y: $scope.num
                                });
                            }
                            return data;
                        }())
                    }]
                });
            });
        }());


    }


})();
